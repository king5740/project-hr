<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 12/5/2022
  Time: 12:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="../CSS/WelcomePageStyle.css"/>

</head>
<body>
<ul>
    <c:forEach items="${pages}" var="s">
        <li><a href=${s.url}>${s.page}</a></li>
    </c:forEach>
    <li><a href="/long-out">Long out</a></li>
</ul>
</br>
</body>
</html>
