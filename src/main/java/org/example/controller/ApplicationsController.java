package org.example.controller;

import org.example.payload.ApplicationDTO;
import org.example.repository.ApplicationRepository;
import org.example.repository.ApplicationRepositoryImpl;
import org.example.service.ApplicationService;
import org.example.service.ApplicationServiceImpl;
import org.example.service.JobService;
import org.example.utils.Pages;
import org.example.utils.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(URI.APPLICATIONS)
public class ApplicationsController extends HttpServlet {
    ApplicationService applicationService = new ApplicationServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            List<ApplicationDTO> applications = applicationService.getApplications(req, resp);
            req.setAttribute("applications", applications);
            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.APPLICATIONS_PAGE);
            dispatcher.forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("message", e.getMessage());
            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.RESPONSE_PAGE);
            dispatcher.forward(req, resp);
        }
    }
}
