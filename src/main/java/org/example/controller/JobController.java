package org.example.controller;

import org.example.payload.JobDTO;
import org.example.service.JobService;
import org.example.service.JobServiceImpl;
import org.example.utils.Pages;
import org.example.utils.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(value = URI.FIND_JOB)
public class JobController extends HttpServlet {
    private JobService jobService = new JobServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<JobDTO> jobs = jobService.getJobs(req, resp);

        req.setAttribute("jobs", jobs);

        RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.FIND_JOB_PAGE);
        dispatcher.forward(req, resp);

    }
}
