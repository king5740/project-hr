package org.example.controller;

import org.example.utils.Pages;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/long-out")
public class LongOut extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] array = request.getCookies();
        for (int i = 0; i < array.length; i++) {
            if (array[i].getName().equals("EMAIl")) {
                array[i].setMaxAge(0);
                resp.addCookie(array[i]);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(Pages.LOGIN_PAGE);
        dispatcher.forward(request, resp);

    }
}
