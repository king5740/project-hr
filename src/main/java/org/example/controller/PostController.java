package org.example.controller;

import org.example.service.JobService;
import org.example.service.JobServiceImpl;
import org.example.utils.Pages;
import org.example.utils.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(URI.POST_JOB)
public class PostController extends HttpServlet {
    private JobService jobService = new JobServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.POST_JOB_PAGE);

        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {

            jobService.saveJoB(req, resp);

            req.setAttribute("message", "Job added successfully");

            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.RESPONSE_PAGE);
            dispatcher.forward(req, resp);

        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());

            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.POST_JOB_PAGE);
            dispatcher.forward(req, resp);
        }
    }
}
