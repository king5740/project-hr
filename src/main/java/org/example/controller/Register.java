package org.example.controller;

import org.example.entity.enums.Role;
import org.example.service.UserServiceImpl;
import org.example.utils.Pages;
import org.example.utils.RolePages;
import org.example.utils.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(URI.REGISTER)
public class Register extends HttpServlet {
    private UserServiceImpl userServiceImpl = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.REGISTER_PAGE);

        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            Role role = userServiceImpl.registrationUser(req, resp);

            switch (role) {
                case HR:
                    req.setAttribute("pages", RolePages.HR_PAGES);
                    break;
                case USER:
                    req.setAttribute("pages", RolePages.USER_PAGES);
                    break;
                case ADMIN:
                    req.setAttribute("pages", RolePages.ADMIN_PAGES);
                    break;
            }

            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.HOME_PAGE);
            dispatcher.forward(req, resp);


        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());

            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.REGISTER_PAGE);
            dispatcher.forward(req, resp);
        }
    }
}
