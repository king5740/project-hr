package org.example.controller;

import org.example.payload.JobDTO;
import org.example.repository.JobRepository;
import org.example.repository.JobRepositoryImpl;
import org.example.service.JobService;
import org.example.service.JobServiceImpl;
import org.example.utils.Message;
import org.example.utils.Pages;
import org.example.utils.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = URI.RESPOND)
public class RespondController extends HttpServlet {
    JobService jobService = new JobServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            int id = Integer.parseInt(req.getParameter("id"));

            JobDTO job = jobService.getJobForRespond(id);

            req.setAttribute("job", job);

            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.RESPOND_JOB_PAGE);
            dispatcher.forward(req, resp);

        } catch (Exception e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.RESPONSE_PAGE);
            req.setAttribute("message", e.getMessage());
            dispatcher.forward(req, resp);
        }
    }
    //todo respond

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            jobService.saveRespond(req, resp);

            req.setAttribute("message", Message.APPLICATION_SAVED_SUCCESSFULLY);


        } catch (Exception e) {
            req.setAttribute("message", e.getMessage());
        } finally {
            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.RESPONSE_PAGE);
            dispatcher.forward(req, resp);
        }
    }
}
