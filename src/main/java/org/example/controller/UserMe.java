package org.example.controller;

import org.example.payload.UserDTO;
import org.example.service.UserService;
import org.example.service.UserServiceImpl;
import org.example.utils.Pages;
import org.example.utils.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(URI.USER_ME)
public class UserMe extends HttpServlet {
    private UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            UserDTO userDTO = userService.userMe(req, resp);
            req.setAttribute("user", userDTO);

            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.USER_ME_PAGE);
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            req.setAttribute("message", e.getMessage());
            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.RESPONSE_PAGE);
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        userService.editUserMe(req, resp);
    }
}
