package org.example.entity;

import lombok.*;
import org.example.entity.enums.ApplicationStatus;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Application {
    private int id;

    private String coverLetter;

    private User applicant;

    private Job job;

    private ApplicationStatus status;

    private Timestamp created_at;

    private Timestamp edited_at;
}
