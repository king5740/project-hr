package org.example.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Company {
    private int id;

    private String name;

    private String description;

}
