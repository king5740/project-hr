package org.example.entity;

import lombok.*;
import org.example.entity.enums.EducationLevel;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Education {
    private int id;

    private String name;

    private EducationLevel level;
}
