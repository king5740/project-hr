package org.example.entity;

import lombok.*;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Experience {

    private int id;

    private String name;

    private String description;

    private Timestamp created_at;
}
