package org.example.entity;

import lombok.*;
import org.example.entity.enums.JobStatus;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Job {

    private Integer id;

    private String title;

    private String description;

    private Integer salary;

    private User createdBy;

    private JobStatus status;

    private Timestamp createdAt;

    private Timestamp edited_at;
}
