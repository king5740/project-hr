package org.example.entity;

import lombok.*;
import org.example.entity.enums.Role;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    private Integer id;

    private String email;

    private String password;

    private String name;

    private Role role;

    private Education education;

    private Company company;

    private Experience experience;

    private List<Job> jobs;

    private List<Application> applications;


}
