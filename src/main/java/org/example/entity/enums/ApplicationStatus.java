package org.example.entity.enums;

public enum ApplicationStatus {
    NEW,
    INTERVIEWING,
    REJECTED,
    ACCEPTED
}
