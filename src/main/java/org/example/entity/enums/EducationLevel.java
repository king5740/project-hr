package org.example.entity.enums;

public enum EducationLevel {
    HIGH,
    BACHELOR,
    PHD,
    MASTER
}
