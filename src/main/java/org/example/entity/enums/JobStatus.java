package org.example.entity.enums;

public enum JobStatus {
    ACTIVE,
    NOT_ACTIVE
}
