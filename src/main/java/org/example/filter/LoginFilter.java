package org.example.filter;

import org.example.entity.User;
import org.example.entity.enums.Role;
import org.example.repository.UserRepository;
import org.example.repository.UserRepositoryImpl;
import org.example.utils.Pages;
import org.example.utils.URI;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@WebFilter(filterName = "postJobFilter", urlPatterns = {
        URI.POST_JOB, URI.HOME, URI.USER_ME, URI.RESPOND})

public class LoginFilter implements Filter {
    UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;


        String email = getEmail(req.getCookies());

        Optional<User> userOptional = userRepository.findByEmail(email);
        String password = getPassword(req.getCookies());

        if (userOptional.isEmpty() || password.length() == 0 || email.length() == 0) {
            req.setAttribute("error", "login qiling");

            RequestDispatcher dispatcher = req.getRequestDispatcher(Pages.LOGIN_PAGE);
            dispatcher.forward(req, servletResponse);
        }

        filterChain.doFilter(req, servletResponse);

    }

    private String getEmail(Cookie[] cookies) {
        String email = "";
        if (Objects.nonNull(cookies))
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("EMAIL")) {
                    email = cookie.getValue();
                    break;
                }
            }


        return email;
    }

    private String getPassword(Cookie[] cookies) {
        String password = "";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("PASSWORD")) {
                password = cookie.getValue();
                break;
            }
        }
        return password;
    }
}