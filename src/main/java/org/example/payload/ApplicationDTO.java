package org.example.payload;

import lombok.*;
import org.example.entity.Job;
import org.example.entity.enums.ApplicationStatus;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ApplicationDTO {
    private int id;

    private String coverLetter;

    private String applicantInfo;

    private String jobTitle;

    private ApplicationStatus status;

    private Timestamp edited_at;
}
