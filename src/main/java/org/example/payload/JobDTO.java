package org.example.payload;

import lombok.*;
import org.example.entity.User;
import org.example.entity.enums.JobStatus;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobDTO {
    private Integer id;

    private String title;

    private String description;

    private Integer salary;

    private String createdBy;

    private JobStatus status;

    private Timestamp edited_at;
}
