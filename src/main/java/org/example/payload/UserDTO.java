package org.example.payload;

import lombok.*;
import org.example.entity.Education;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UserDTO {
    private String email;
    private String password;

    private String name;
    private String educationName;
    private String educationLevel;
    private String experience;
    private String description;
}
