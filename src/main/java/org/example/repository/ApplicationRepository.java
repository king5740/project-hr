package org.example.repository;

import org.example.entity.Application;
import org.example.entity.Job;
import org.example.entity.User;
import org.example.payload.JobDTO;

import java.util.List;
import java.util.Optional;

public interface ApplicationRepository {
    boolean save(Application application);

    void delete(int id);

    Optional<Application> findById(int id);

    List<JobDTO> findAll();

    List<Application> findAllByUser(int id);

    boolean editFind(Job application);


    List<Application> findAllApplicationByHR(Integer id);
}
