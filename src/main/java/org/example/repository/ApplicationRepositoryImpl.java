package org.example.repository;

import org.example.entity.*;
import org.example.entity.enums.ApplicationStatus;
import org.example.entity.enums.EducationLevel;
import org.example.entity.enums.Role;
import org.example.payload.JobDTO;
import org.example.service.DBConnection;
import org.example.service.ServiceException;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ApplicationRepositoryImpl implements ApplicationRepository {
    String SAVE_APPLICATION = "insert into application(job, user_id, cover_letter, created_at, edited_at,status)\n" +
            "values (?,?,?,?,?,?);";

    String GET_USER_ROLE_APPLICATIONS = "select application.id           as applicationId,\n" +
            "       application.cover_letter as coverLetter,\n" +
            "       application.edited_at    as editedAt,\n" +
            "       application.status       as status,\n" +
            "       hr.id                    as hrID,\n" +
            "       applicant.id             as applicantId,\n" +
            "       hr.name                  as hrName,\n" +
            "       j.title                  as jobTitle\n" +
            "from application\n" +
            "         join job j\n" +
            "              on j.id = application.job\n" +
            "         join users as hr on hr.id = j.created_by\n" +
            "         join users as applicant on applicant.id = application.user_id\n" +
            "where applicant.id = ?\n" +
            "order by j.edited_at;";

    String GET_APPLICATION_HR_BY_ID = "WITH userInfo AS (select users.id        as applicant_id,\n" +
            "                         users.name      as applicantName,\n" +
            "                         users.email     as applicantEmail,\n" +
            "                         e.name          as education,\n" +
            "                         e.level         as level,\n" +
            "                         ex.company_name as experience,\n" +
            "                         ex.description  as description\n" +
            "                  from users\n" +
            "                           LEFT JOIN education e on users.id = e.user_id\n" +
            "                           LEFT JOIN\n" +
            "                       experience ex\n" +
            "                       on users.id = ex.user_id)\n" +
            "\n" +
            "\n" +
            "select a.id,\n" +
            "       userInfo.*,\n" +
            "       job.title,\n" +
            "       a.user_id,\n" +
            "       job.created_by,\n" +
            "       a.status,\n" +
            "       a.edited_at \n" +
            "from job\n" +
            "         join application a on job.id = a.job\n" +
            "         left join userInfo on a.user_id = userInfo.applicant_id\n" +
            "where job.created_by = ?\n" +
            "order by job.title";

    @Override
    public boolean save(Application application) {
        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SAVE_APPLICATION);

            statement.setInt(1, application.getJob().getId());
            statement.setInt(2, application.getApplicant().getId());
            statement.setString(3, application.getCoverLetter());
            statement.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()));
            statement.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
            statement.setString(6, application.getStatus().name());

            return statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw ServiceException.throwExc(e.getMessage());
            } finally {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Optional<Application> findById(int id) {
        return Optional.empty();
    }

    @Override
    public List<JobDTO> findAll() {
        return null;
    }

    @Override
    public List<Application> findAllByUser(int id) {
        List<Application> applications = new ArrayList<>();

        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(GET_USER_ROLE_APPLICATIONS);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                applications.add(mapResultSetToApplication(resultSet));
            }
            return applications;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {

            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public boolean editFind(Job application) {
        return false;
    }

    public List<Application> findAllApplicationByHR(Integer id) {
        List<Application> applications = new ArrayList<>();

        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_APPLICATION_HR_BY_ID);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                applications.add(mapResultSetToApplicationWithDetailer(resultSet));
            }
            return applications;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {

            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }


    }

    private Application mapResultSetToApplicationWithDetailer(ResultSet resultSet) {
        try {
            //applicant
            int applicant_id = resultSet.getInt("applicant_id");
            String applicantEmail = resultSet.getString("applicantEmail");
            String applicantName = resultSet.getString("applicantName");

            User applicant = User.builder()
                    .id(applicant_id)
                    .email(applicantEmail)
                    .name(applicantName)
                    .education(
                            Education.builder()
                                    .level(EducationLevel.valueOf(resultSet.getString("level")))
                                    .name(resultSet.getString("education")).build())
                    .experience(
                            Experience.builder()
                                    .name(resultSet.getString("experience"))
                                    .description("description")
                                    .build()
                    )
                    .build();

            return Application.builder()
                    .job(Job.builder()
                            .title(resultSet.getString("title")).
                            createdBy(User.builder().id(resultSet.getInt("created_by")).build()).build())
                    .edited_at(resultSet.getTimestamp("edited_at"))
                    .status(ApplicationStatus.valueOf(resultSet.getString("status")))
                    .applicant(applicant)
                    .build();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Application mapResultSetToApplication(ResultSet resultSet) {
        try {
            User applicant = User.builder().role(Role.USER).id(resultSet.getInt("applicantId")).build();
            User hr = User.builder().role(Role.HR).id(resultSet.getInt("hrID")).build();

            int applicationId = resultSet.getInt("applicationId");
            String coverLetter = resultSet.getString("coverLetter");
            Timestamp editedAt = resultSet.getTimestamp("editedAt");
            ApplicationStatus applicationStatus = ApplicationStatus.valueOf(resultSet.getString("status"));
            String jobTitle = resultSet.getString("jobTitle");

            Job job = Job.builder().title(jobTitle).createdBy(hr).build();


            return Application.builder()
                    .id(applicationId)
                    .coverLetter(coverLetter)
                    .status(applicationStatus)
                    .applicant(applicant)
                    .edited_at(editedAt)
                    .job(job).build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
