package org.example.repository;

import org.example.entity.Job;
import org.example.entity.User;
import org.example.payload.JobDTO;

import java.util.List;
import java.util.Optional;

public interface JobRepository {
    boolean save(Job job);

    void delete(int id);

    Optional<Job> findById(int id);

    List<JobDTO> findAll();

    boolean editJob(Job job);


}
