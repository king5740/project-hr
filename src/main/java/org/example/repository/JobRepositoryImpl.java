package org.example.repository;

import org.example.entity.Job;
import org.example.entity.User;
import org.example.entity.enums.JobStatus;
import org.example.payload.JobDTO;
import org.example.service.DBConnection;
import org.example.service.ServiceException;
import org.example.utils.Message;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JobRepositoryImpl implements JobRepository {
    private final String SAVE_JOB = "INSERT INTO job (title,description,salary,status,created_by,created_at,edited_at)" +
            ("VALUES (?,?,?,?,?,?,?)");
    private final String GET_ALL_JOBS = "SELECT job.*, u.name\n" +
            "FROM job\n" +
            "         join users u on job.created_by = u.id\n" +
            "where job.status = 'ACTIVE'";

    private final String FIND_JOB_BY_ID = "SELECT job.*, u.id as user_id, u.name\n" +
            "FROM job\n" +
            "         join users u on job.created_by = u.id\n" +
            "where job.id = ?;";

    @Override
    public boolean save(Job job) {
        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SAVE_JOB);
            statement.setString(1, job.getTitle());
            statement.setString(2, job.getDescription());
            statement.setInt(3, job.getSalary());
            statement.setString(4, job.getStatus().name());
            statement.setInt(5, job.getCreatedBy().getId());
            statement.setTimestamp(6, Timestamp.valueOf(LocalDateTime.now()));
            statement.setTimestamp(7, Timestamp.valueOf(LocalDateTime.now()));

            if (statement.execute()) {
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        return false;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Optional<Job> findById(int id) {
        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(FIND_JOB_BY_ID);

            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Job job = mapResultSetToJob(resultSet);
                return Optional.of(job);
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    @Override
    public List<JobDTO> findAll() {
        List<JobDTO> jobs = new ArrayList<>();

        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(GET_ALL_JOBS);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                JobDTO jobDTO = mapResultSetToDTO(resultSet);
                jobs.add(jobDTO);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return jobs;
    }


    @Override
    public boolean editJob(Job job) {
        return false;
    }

    private JobDTO mapResultSetToDTO(ResultSet resultSet) throws SQLException {

        int id = resultSet.getInt("id");
        String title = resultSet.getString("title");
        String description = resultSet.getString("description");
        int salary = resultSet.getInt("salary");
        JobStatus status = JobStatus.valueOf(resultSet.getString("status"));
        String hrName = resultSet.getString("name");
        Timestamp timestamp = resultSet.getTimestamp("edited_at");

        JobDTO jobDTO = JobDTO.builder()
                .id(id)
                .title(title)
                .description(description)
                .salary(salary)
                .status(status)
                .createdBy(hrName)
                .edited_at(timestamp).build();

        return jobDTO;
    }

    private Job mapResultSetToJob(ResultSet resultSet) {
        try {
            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            String description = resultSet.getString("description");
            int salary = resultSet.getInt("salary");
            JobStatus status = JobStatus.valueOf(resultSet.getString("status"));
            Timestamp edited_at = resultSet.getTimestamp("edited_at");
            Timestamp created_at = resultSet.getTimestamp("created_at");


            int user_id = resultSet.getInt("user_id");
            String name = resultSet.getString("name");

            User user = User.builder().id(user_id).name(name).build();

            Job build = Job.builder()
                    .id(id)
                    .title(title)
                    .description(description)
                    .createdBy(user)
                    .status(status)
                    .salary(salary)
                    .createdAt(created_at)
                    .edited_at(edited_at)
                    .build();
            return build;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
