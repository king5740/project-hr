package org.example.repository;

import org.example.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    boolean save(User user);

    void delete(int id);

    Optional<User> findById(int id);

    List<User> findAll();

    boolean editUser(User user);

    boolean existByEmail(String email);

    boolean existsByEmailWithId(String email, int id);

    Optional<User> findByEmail(String email);

}
