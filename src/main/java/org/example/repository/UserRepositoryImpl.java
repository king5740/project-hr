package org.example.repository;

import lombok.NonNull;
import org.example.entity.Education;
import org.example.entity.Experience;
import org.example.entity.User;
import org.example.entity.enums.EducationLevel;
import org.example.entity.enums.Role;
import org.example.service.DBConnection;
import org.example.service.ServiceException;
import org.example.utils.Message;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UserRepositoryImpl implements UserRepository {
    private final String SAVE_USER_QUERY = "INSERT INTO users (email, password, role, created_at, edited_at, name, active)\n" +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";

    private final String CHECK_EMAIL_WITH_ID = "select *\n" +
            "from users\n" +
            "where id != ?\n" +
            "  and email = ?;";

    private final String FIND_BY_EMAIL = "select u.id,\n" +
            "       u.name,\n" +
            "       u.email,\n" +
            "       u.password,\n" +
            "       u.role as role,\n" +
            "       e.id            as educationId,\n" +
            "       e.name          as educationName,\n" +
            "       e.level         as educationLevel,\n" +
            "       ex.id           as experienceId,\n" +
            "       ex.company_name as experience,\n" +
            "       ex.description  as description\n" +
            "from users as u\n" +
            "         left join education e on u.id = e.user_id\n" +
            "         left join experience ex on u.id = ex.user_id\n" +
            "where u.email=?;";

    private final String EXISTS_BY_EMAIL = "SELECT COUNT(id) AS count FROM users WHERE email=?";

    @Override
    public boolean save(@NonNull User user) {

        String email = user.getEmail();

        if (existByEmail(email)) {
            throw ServiceException.throwExc(Message.EMAIL_ALREADY_TAKEN);
        }

        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SAVE_USER_QUERY);
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getRole().name());
            statement.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()));
            statement.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
            statement.setString(6, user.getName());
            statement.setBoolean(7, true);

            if (statement.execute()) {
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return false;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Optional<User> findById(int id) {
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public boolean editUser(User user) {
        return false;
    }

    @Override
    public boolean existByEmail(String email) {
        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(EXISTS_BY_EMAIL);
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int count = resultSet.getInt("count");
                if (count == 0)
                    return false;
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public boolean existsByEmailWithId(String email, int id) {
        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {

            PreparedStatement statement = connection.prepareStatement(CHECK_EMAIL_WITH_ID);
            statement.setInt(1, id);
            statement.setString(2, email);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                if (Objects.nonNull(resultSet.getString("email"))) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {

            PreparedStatement statement = connection.prepareStatement(FIND_BY_EMAIL);
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                User user = mappingUserFromResultSet(resultSet);
                return Optional.of(user);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }

        return Optional.empty();
    }

    User mappingUserFromResultSet(ResultSet resultSet) {
        try {
            Experience experience = Experience.builder().build();
            Education education = Education.builder().build();

            if (Objects.nonNull(resultSet.getString("experience"))) {
                experience = Experience.builder()
                        .id(resultSet.getInt(resultSet.getInt("experienceId")))
                        .name(resultSet.getString("experience"))
                        .description(resultSet.getString("description")).build();
            }

            if (Objects.nonNull(resultSet.getString("educationName"))) {
                education = Education.builder()
                        .id(resultSet.getInt("educationId"))
                        .name(resultSet.getString("educationName"))
                        .level(EducationLevel.valueOf(resultSet.getString("educationLevel")))
                        .build();
            }
            return User.builder()
                    .id(resultSet.getInt("id"))
                    .email(resultSet.getString("email"))
                    .name(resultSet.getString("name"))
                    .password(resultSet.getString("password"))
                    .role(Role.valueOf(resultSet.getString("role")))
                    .experience(experience)
                    .education(education)
                    .build();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
