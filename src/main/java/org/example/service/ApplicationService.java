package org.example.service;

import org.example.payload.ApplicationDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface ApplicationService {
    List<ApplicationDTO> getApplications(HttpServletRequest req, HttpServletResponse resp);
}
