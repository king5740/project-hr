package org.example.service;

import org.example.entity.Application;
import org.example.entity.User;
import org.example.entity.enums.Role;
import org.example.payload.ApplicationDTO;
import org.example.repository.ApplicationRepository;
import org.example.repository.ApplicationRepositoryImpl;
import org.example.repository.UserRepository;
import org.example.repository.UserRepositoryImpl;
import org.example.utils.Message;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ApplicationServiceImpl implements ApplicationService {
    private final ApplicationRepository applicationRepository = new ApplicationRepositoryImpl();

    private final UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public List<ApplicationDTO> getApplications(HttpServletRequest req, HttpServletResponse resp) {

        String email = getEmailFromCookie(req.getCookies());
        Optional<User> optionalUser = userRepository.findByEmail(email);
        User user = optionalUser.orElseThrow(() -> ServiceException.throwExc(Message.LOGIN_REQUIRED));

        if (user.getRole().equals(Role.USER)) {
            return mapListToDTo(applicationRepository.findAllByUser(user.getId()));
        } else if (user.getRole().equals(Role.HR)) {
            return applicationRepository.findAllApplicationByHR(user.getId()).stream()
                    .map(this::mapWithDetails).collect(Collectors.toList());
        }
        return null;

    }

    private String mapUserToInfo(User user) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(user.getName() + "\n");
        stringBuilder.append(user.getEducation().getName() + "\n");
        stringBuilder.append(user.getEducation().getLevel().name() + "\n");
        stringBuilder.append("\n");
        stringBuilder.append(user.getExperience().getName() + "\n");
        stringBuilder.append(user.getExperience().getDescription() + "\n");

        return stringBuilder.toString();
    }

    private List<ApplicationDTO> mapListToDTo(List<Application> applications) {

        if (applications.size() > 0) {
            return applications.stream().map(this::mapApplicationToDTO).collect(Collectors.toList());
        }

        return new LinkedList<>();
    }

    private ApplicationDTO mapApplicationToDTO(Application application) {
        return ApplicationDTO.builder()
                .id(application.getId())
                .status(application.getStatus())
                .edited_at(application.getEdited_at())
                .jobTitle(application.getJob().getTitle())
                .coverLetter(application.getCoverLetter()).build();
    }

    private String getEmailFromCookie(Cookie[] cookies) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("EMAIL"))
                return cookie.getValue();
        }
        throw ServiceException.throwExc(Message.LOGIN_REQUIRED);
    }

    private ApplicationDTO mapWithDetails(Application application) {
        return ApplicationDTO.builder()
                .id(application.getId())
                .status(application.getStatus())
                .edited_at(application.getEdited_at())
                .jobTitle(application.getJob().getTitle())
                .applicantInfo(
                        mapUserToInfo(application.getApplicant())
                ).build();
    }
}
