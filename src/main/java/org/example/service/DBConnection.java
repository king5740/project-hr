package org.example.service;

import java.sql.*;

public class DBConnection {
    private final String dbUrl = "jdbc:postgresql://localhost/project_hr";
    private final String dbUser = "postgres";
    private final String dbPassword = "root123";

    public ResultSet execute(String query) {
        try {
            Class.forName("org.postgresql.Driver");

            Connection con = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            con.close();
            return resultSet;
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");

            Connection con = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);

            return con;
        } catch (SQLException e) {
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }


    public void insert(String tableName, String fields, String values) {
        try {
            Connection con = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
            Statement statement = con.createStatement();
            statement.executeUpdate("INSERT INTO " + tableName + " (" + fields + ") VALUES (" + values + ");");
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
