package org.example.service;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@WebListener
public class DeployListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        List<String> queries = getTableQueries();


        DBConnection dbConnection = new DBConnection();
        Connection connection = dbConnection.getConnection();

        try {
            Statement statement = connection.createStatement();
            for (String query : queries) {

                statement.execute(query);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }

    }

    private List<String> getTableQueries() {
        List<String> queries = new ArrayList<>();

        //table users
        queries.add("CREATE TABLE IF NOT EXISTS users(\n" +
                "    id int GENERATED ALWAYS AS IDENTITY,\n" +
                "    email  VARCHAR  UNIQUE  NOT NULL,\n" +
                "    password  VARCHAR NOT NULL,\n" +
                "    name VARCHAR NOT NULL," +
                "    role VARCHAR  NOT NULL,\n" +
                "    active boolean NOT NULL,\n" +
                "    created_at TIMESTAMP NOT NULL,\n" +
                "    edited_at TIMESTAMP NOT NULL,\n" +
                "   \n" +
                "    PRIMARY KEY(id)" +
                ");");

        //table experience
        queries.add("CREATE TABLE IF NOT EXISTS experience(\n" +
                "                                         id int GENERATED ALWAYS AS IDENTITY,\n" +
                "                                         company_name  VARCHAR    NOT NULL,\n" +
                "                                         user_id int NOT NULL,\n" +
                "                                         description  TEXT NOT NULL,\n" +
                "                                         PRIMARY KEY(id),\n" +
                "                                         CONSTRAINT fk_user_id\n" +
                "                                             FOREIGN KEY(user_id)\n" +
                "                                                 REFERENCES users(id)\n" +
                ");");

        // table company
        queries.add("CREATE TABLE IF NOT EXISTS company(\n" +
                "id int GENERATED ALWAYS AS IDENTITY,\n" +
                "company  VARCHAR    NOT NULL,\n" +
                "description  TEXT NOT NULL,\n" +
                "registered_time TIMESTAMP,\n" +
                "PRIMARY KEY(id)\n" +
                ");");
        // table education
        queries.add("CREATE TABLE IF NOT EXISTS education(\n" +
                "id int GENERATED ALWAYS AS IDENTITY,\n" +
                "name VARCHAR    NOT NULL,\n" +
                "level  VARCHAR NOT NULL,\n" +
                "user_id int NOT NULL," +
                "created_at TIMESTAMP NOT NULL,\n" +
                "edited_at TIMESTAMP NOT NULL,\n" +
                "PRIMARY KEY(id),\n" +
                "    CONSTRAINT fk_user_id\n" +
                "    FOREIGN KEY(user_id)   \n" +
                "    REFERENCES users(id)\n" +
                ");");

        //table users


        //table job
        queries.add("CREATE TABLE IF NOT EXISTS job(\n" +
                "    id int GENERATED ALWAYS AS IDENTITY,\n" +
                "    title  VARCHAR   NOT NULL,\n" +
                "    description  VARCHAR NOT NULL,\n" +
                "    salary FLOAT  NOT NULL,\n" +
                "    status varchar NOT NULL,\n" +
                "    created_by int,\n" +
                "    created_at TIMESTAMP NOT NULL,\n" +
                "    edited_at TIMESTAMP NOT NULL,\n" +
                "\n" +
                "    PRIMARY KEY(id),   \n" +
                "    CONSTRAINT fk_created_by  \n" +
                "    FOREIGN KEY(created_by)   \n" +
                "    REFERENCES users(id)\n" +
                ");");

        //table application
        queries.add("CREATE TABLE IF NOT EXISTS application(\n" +
                "    id int GENERATED ALWAYS AS IDENTITY,\n" +
                "    job  int   NOT NULL,\n" +
                "    user_id int NOT NULL,\n" +
                "    cover_letter text NOT NULL,\n" +
                "    status varchar NOT NULL,\n" +
                "    created_at TIMESTAMP NOT NULL,\n" +
                "    edited_at TIMESTAMP NOT NULL,\n" +
                "\n" +
                "    PRIMARY KEY(id),   \n" +
                "    \n" +
                "    CONSTRAINT fk_user \n" +
                "    FOREIGN KEY(user_id)   \n" +
                "    REFERENCES USERS(id),\n" +
                "    \n" +
                "    CONSTRAINT fk_job \n" +
                "    FOREIGN KEY(job)   \n" +
                "    REFERENCES job(id),\n" +
                "    \n" +
                "    UNIQUE (job, user_id)\n" +
                "   \n" +
                ");\n");

        //table saved_job
        queries.add("CREATE TABLE IF NOT EXISTS saved_job(\n" +
                "    id int GENERATED ALWAYS AS IDENTITY,\n" +
                "    job  int   NOT NULL,\n" +
                "   user_id int NOT NULL,\n" +
                "  \n" +
                "    CONSTRAINT fk_user \n" +
                "    FOREIGN KEY(user_id)   \n" +
                "    REFERENCES USERS(id),\n" +
                "    \n" +
                "    CONSTRAINT fk_job \n" +
                "    FOREIGN KEY(job)   \n" +
                "    REFERENCES job(id),\n" +
                "    \n" +
                "     UNIQUE (job, user_id)\n" +
                ");");
        return queries;

    }


}
