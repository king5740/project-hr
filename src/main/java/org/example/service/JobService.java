package org.example.service;

import org.example.payload.JobDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface JobService {

    void saveJoB(HttpServletRequest request, HttpServletResponse response);

    List<JobDTO> getJobs(HttpServletRequest req, HttpServletResponse resp);

    JobDTO getJobForRespond(int id);

    void saveRespond(HttpServletRequest req, HttpServletResponse resp);
}
