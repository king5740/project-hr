package org.example.service;

import org.example.entity.Application;
import org.example.entity.Job;
import org.example.entity.User;
import org.example.entity.enums.ApplicationStatus;
import org.example.entity.enums.JobStatus;
import org.example.payload.JobDTO;
import org.example.repository.*;
import org.example.utils.Message;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class JobServiceImpl implements JobService {
    private JobRepository jobRepository = new JobRepositoryImpl();
    private UserRepository userRepository = new UserRepositoryImpl();
    private ApplicationRepository applicationRepository = new ApplicationRepositoryImpl();

    @Override
    public void saveJoB(HttpServletRequest request, HttpServletResponse response) {
        String title = request.getParameter("title");

        String description = request.getParameter("description");

        Integer salary = Integer.parseInt(request.getParameter("salary"));

        Cookie[] cookies = request.getCookies();

        String email = "";

        email = getEmail(cookies, email);

        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> ServiceException.throwExc(Message.EMAIL_INVALID));

        Job job = Job.builder()
                .title(title)
                .description(description)
                .createdBy(user)
                .salary(salary)
                .status(JobStatus.ACTIVE)
                .build();

        jobRepository.save(job);
    }

    @Override
    public List<JobDTO> getJobs(HttpServletRequest req, HttpServletResponse resp) {
        List<JobDTO> all = jobRepository.findAll();
        return all;
    }

    @Override
    public JobDTO getJobForRespond(int id) {
        Optional<Job> byId = jobRepository.findById(id);

        Job job = byId.orElseThrow(() -> ServiceException.throwExc(Message.JOB_ID_INVALID));

        if (job.getStatus().equals(JobStatus.ACTIVE)) {
            return mapJobToDTO(job);
        } else {
            throw ServiceException.throwExc(Message.JOB_ARCHIVED);
        }
    }

    @Override
    public void saveRespond(HttpServletRequest req, HttpServletResponse resp) {
        String email = getEmailFromCookie(req.getCookies());
        String coverLetter = req.getParameter("coverLetter");
        int jobId = Integer.parseInt(req.getParameter("jobId"));

        Optional<Job> byId = jobRepository.findById(jobId);
        Job job = byId.orElseThrow(() -> ServiceException.throwExc(Message.JOB_ID_INVALID));


        Optional<User> byEmail = userRepository.findByEmail(email);
        User user = byEmail.orElseThrow(() -> ServiceException.throwExc(Message.EMAIL_INVALID));

        Application application = Application.builder()
                .job(job)
                .applicant(user)
                .status(ApplicationStatus.NEW)
                .coverLetter(coverLetter)
                .build();

        applicationRepository.save(application);

    }

    private String getEmailFromCookie(Cookie[] cookies) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("EMAIL"))
                return cookie.getValue();
        }
        throw ServiceException.throwExc(Message.LOGIN_REQUIRED);
    }

    private String getEmail(Cookie[] cookies, String email) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("EMAIL")) {
                email = cookie.getValue();
                break;
            }
        }

        if (email.length() == 0)
            throw ServiceException.throwExc(Message.EMAIL_INVALID);
        return email;
    }

    private JobDTO mapJobToDTO(Job job) {
        return JobDTO.builder()
                .id(job.getId())
                .title(job.getTitle())
                .description(job.getDescription())
                .edited_at(job.getEdited_at())
                .status(job.getStatus())
                .createdBy(job.getCreatedBy().getName())
                .salary(job.getSalary()).build();
    }
}
