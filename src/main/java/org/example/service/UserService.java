package org.example.service;

import org.example.entity.enums.Role;
import org.example.payload.UserDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserService {
    Role registrationUser(HttpServletRequest req, HttpServletResponse resp);

    Role login(HttpServletRequest req, HttpServletResponse resp);

    UserDTO userMe(HttpServletRequest req, HttpServletResponse resp);

    void editUserMe(HttpServletRequest req, HttpServletResponse resp);
}
