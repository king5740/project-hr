package org.example.service;

import org.example.entity.Education;
import org.example.entity.Experience;
import org.example.entity.User;
import org.example.entity.enums.EducationLevel;
import org.example.entity.enums.Role;
import org.example.payload.UserDTO;
import org.example.repository.UserRepository;
import org.example.repository.UserRepositoryImpl;
import org.example.utils.Message;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public Role registrationUser(HttpServletRequest req, HttpServletResponse resp) {
        String email = req.getParameter("email");
        Role role = Role.valueOf(req.getParameter("role"));
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        String prePassword = req.getParameter("prePassword");

        if (userRepository.existByEmail(email)) {
            throw ServiceException.throwExc(Message.EMAIL_ALREADY_TAKEN);
        }
        if (!passwordValidation(password, prePassword)) {
            throw ServiceException.throwExc(Message.PASSWORD_INVALID);
        }

        User user = User.builder()
                .email(email)
                .password(password)
                .name(name)
                .role(role)
                .build();
        userRepository.save(user);

        setCookie(resp, email, password);

        return role;
    }

    @Override
    public Role login(HttpServletRequest req, HttpServletResponse resp) {
        String email = req.getParameter("email");

        String password = req.getParameter("password");

        if (Objects.isNull(email)
                || Objects.isNull(password))
            throw ServiceException.throwExc(Message.EMAIL_OR_PASSWORD_INVALID);

        Optional<User> userOptional = userRepository.findByEmail(email);

        User user = userOptional.orElseThrow(() -> ServiceException.throwExc(Message.EMAIL_OR_PASSWORD_INVALID));

        if (!Objects.equals(user.getPassword(), password)) {
            throw ServiceException.throwExc(Message.EMAIL_OR_PASSWORD_INVALID);
        }

        setCookie(resp, email, password);
        return user.getRole();
    }

    @Override
    public UserDTO userMe(HttpServletRequest req, HttpServletResponse resp) {

        Cookie[] cookies = req.getCookies();
        String email = getEmail(cookies);
        Optional<User> byEmail = userRepository.findByEmail(email);

        User user = byEmail.orElseThrow(() -> ServiceException.throwExc(Message.EMAIL_INVALID));

        return mapUserToDTO(user);

    }

    @Override
    public void editUserMe(HttpServletRequest req, HttpServletResponse resp) {
        String email = getEmail(req.getCookies());

        Optional<User> byEmail = userRepository.findByEmail(email);
        User user = byEmail.orElseThrow(() -> ServiceException.throwExc(Message.EMAIL_INVALID));

        String newEmail = req.getParameter("email");
        if (userRepository.existsByEmailWithId(newEmail, user.getId()))
            throw ServiceException.throwExc(Message.EMAIL_ALREADY_TAKEN);
        String password = req.getParameter("password");

        String name = req.getParameter("name");

        String educationName = req.getParameter("educationName");
        EducationLevel educationLevel = EducationLevel.valueOf(req.getParameter("educationLevel"));


        String experience = req.getParameter("experience");
        String description = req.getParameter("description");

        user.setEmail(newEmail);
        user.setPassword(password);

        Education education = user.getEducation();
        education.setName(educationName);
        education.setLevel(educationLevel);

        user.setEducation(education);

        Experience userExperience = user.getExperience();
        userExperience.setName(educationName);
        userExperience.setName(educationName);
    }

    public String getEmail(Cookie[] cookies) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("EMAIL"))
                return cookie.getValue();
        }
        return ":";
    }

    private UserDTO mapUserToDTO(User user) {
        return UserDTO.builder()
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .educationName(Objects.isNull(user.getEducation()) ? "" : user.getEducation().getName())
                .educationLevel(Objects.isNull(user.getEducation().getLevel()) ? "" : user.getEducation().getLevel().name())
                .experience(Objects.isNull(user.getExperience()) ? "" : user.getExperience().getName())
                .description(Objects.isNull(user.getExperience().getDescription()) ? "" : user.getExperience().getDescription())
                .build();
    }

    private boolean passwordValidation(String password, String prePassword) {
        if (Objects.isNull(password)
                || Objects.isNull(prePassword)
                || !Objects.equals(password, prePassword))
            return false;

        return true;
    }

    private void setCookie(HttpServletResponse response, String email, String password) {
        response.addCookie(new Cookie("EMAIL", email));
        response.addCookie(new Cookie("PASSWORD", password));
    }

}
