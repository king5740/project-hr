package org.example.utils;

public interface Message {
    String EMAIL_ALREADY_TAKEN = "Email is already taken";
    String PASSWORD_INVALID = "Password is invalid";

    String EMAIL_OR_PASSWORD_INVALID = "Password or email is invalid";
    String EMAIL_INVALID = "Email is invalid";

    String JOB_ID_INVALID = "Job id is invalid";

    String JOB_ARCHIVED = "Job  is archived";

    String LOGIN_REQUIRED = "Please login";
    String APPLICATION_SAVED_SUCCESSFULLY = "Applicaton saved successfully";
}
