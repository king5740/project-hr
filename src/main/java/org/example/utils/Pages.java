package org.example.utils;

import java.util.ArrayList;
import java.util.List;

public interface Pages {
    String WELCOME_PAGE = "pages/welcome.jsp";

    String LOGIN_PAGE = "pages/login.jsp";

    String REGISTER_PAGE = "pages/register.jsp";

    String HOME_PAGE = "pages/home.jsp";

    String POST_JOB_PAGE = "pages/postJob.jsp";

    String APPLICATIONS_PAGE = "pages/applications.jsp";

    String APPLICATION_PAGE = "pages/application.jsp";

    String USER_ME_PAGE = "pages/userMe.jsp";

    String FIND_JOB_PAGE = "pages/findJob.jsp";

    String RESPONSE_PAGE = "pages/response.jsp";
    String RESPOND_JOB_PAGE = "pages/respond.jsp";



}
