package org.example.utils;

import java.util.ArrayList;
import java.util.List;

public interface RolePages {
    List<URI_PAGE> HR_PAGES = List.of(
            new URI_PAGE("Home", URI.FIND_JOB),
            new URI_PAGE("Applications", URI.APPLICATIONS),
            new URI_PAGE("Post A Job", URI.POST_JOB),
            new URI_PAGE("ME", URI.USER_ME));

    List<URI_PAGE> ADMIN_PAGES = List.of(
            new URI_PAGE("Home", URI.FIND_JOB),
            new URI_PAGE("ME", URI.USER_ME));

    List<URI_PAGE> USER_PAGES = List.of(
            new URI_PAGE("Home", URI.FIND_JOB),
            new URI_PAGE("Applications", URI.APPLICATIONS),
            new URI_PAGE("FindJob", URI.FIND_JOB),
            new URI_PAGE("ME", URI.USER_ME));
}
