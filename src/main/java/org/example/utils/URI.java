package org.example.utils;

public interface URI {

    String HOME = "/home";

    String LOGIN = "/login";

    String REGISTER = "/register";


    String FIND_JOB = "/find-job";
    String APPLICATIONS = "/applications";
    String POST_JOB = "/post-job";
    String USER_ME = "/user-me";

    String RESPOND = "/respond";



}
