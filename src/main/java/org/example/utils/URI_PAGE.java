package org.example.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class URI_PAGE {
    String page;
    String url;
}
