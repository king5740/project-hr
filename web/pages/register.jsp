<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 12/5/2022
  Time: 12:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="../CSS/RegisterCSS.css"/>

<html>
<head>
    <title>Register</title>
</head>
<body>
<form action="/register" method="post">
    <div class="formLogin">
        <div class="error">
            <h>${error}</h>
        </div>
        <div class="form">
            <input type="email" name="email" placeholder="email" required minlength="8">Email<br>
            <input type="password" name="password" placeholder="password" required minlength>Password<br>
            <input type="password" name="prePassword" placeholder="password again" required>PrePassword<br>
            <input type="text" name="name" placeholder="enter your full name" required>Full name<br>

            <input type="radio" name="role" value="USER" CHECKED>As User<br>
            <input type="radio" name="role" value="HR">As Employee<br>

            <input type="submit" value="Submit">
        </div>
    </div>
</form>

</body>
</html>
